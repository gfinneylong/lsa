# Problem
Find related articles that can be used for reference in fact checking. 

# Considerations

a.	Queries need to be about specific topics or references

b.	The dimensionality will be massive and therefore memory and computationally intensive

c.	The domain of subjects may get so large that it needs to be divide into subdomains

d.	Preferable to have an algorithm that supports both supervised and unsupervised learning

e.	Preferable to have an algorithm that supports adding features 

f.	Preferable to be easy to parallelize 


# Dataset

a.	Started with news space

b.	Looked at Microsoft paraphrase corpus

c.	Settled on Reuters News Article Corpus from UCI machine learning repo

 * http://archive.ics.uci.edu/ml/datasets/Reuters-21578+Text+Categorization+Collection
 * 22,000 news articles from Reuters in 1987, 13,000 Tagged from 135 economic topics
 * Using news articles is a good way to verify the accuracy on the expected type of data

# Implementation Decisions 

1.	Given the size of the English language + Proper Nouns, the large and expanding amount of articles, I wanted to reduce the dimensionality to a minimum while preserving accuracy. **Figures 1 & 2** confirm that a dimensionality reduction is advisable.

1.	Use Latent Semantic Analysis to represent the data – preserves internal relationships on untagged data. Chosen over Support Vector Machines, Naïve Bayes, and Neural Networks. See: http://link.springer.com/chapter/10.1007%2F11893004_51

    2.	TF-IDF

        3.	Provides normalization

        3.	Flexible representation; can add or remove features beyond tf/idf

    2.	Singular Value Decomposition 

        3.	Solve the system of linear relations Tf_Idf = U * S * V^t
        3.	Allows reduction to given dimensionality while preserving most important relationship.
        3.	SVD is very similar to principal component analysis, however doesn’t remove the means from the resultant matrices, and so has better performance on sparse datasets.

    2.	Choice of LSA

        3.	Finds words of similar meaning without them being in search criteria or having pre-defined relationships, due to the underlying principle that words used in the same contexts tends to have the same meaning. This relationship is strong enough to cross languages. 
        3.	No need for dictionary and due to dimensionality reduction, LSA is very tolerant of noise which will be abundant. Noise is further reduced by the decision to do logarithmic normalization over the TF-Idf Matrix. **Figure 8** shows a logarithmic relationship between the number of articles containing a word, and the total word frequency, supporting this normalization. LSA’s tolerance also reduces the need for filtering, leaving room to discover interesting relationships.
        3.	Results of LSA are similar to human judgements: http://lsa.colorado.edu/papers/nips.pdf

1.	Eventually input data probably won’t be well classified, and so K-means clustering is used to group the data. This has the advantage not only of being unsupervised, but also that clusters do not need to be pre-defined, eliminating the need for a training set, which is otherwise used in supervised classification methods. The ‘Elbow Method’ for deriving K is used for its simplicity and visualization.

1.	The article’s title is excluded from the Tf-Idf matrix. An article’s title is generally the text outputted for identification, especially as result of a query, so that that output does not become cluttered. As such, titles are excluded from the Tf-Idf matrix so that the title text does not influence the search. This helps to confirm that the algorithm is finding the deeper relationships within the text, rather than just matching article texts.

# Optimizations

  *	 With the given 13k articles, there are 49k unique words. Although the number of words is finite, there are a large number of possible derivations. This implies that there will be a low proportion of words appearing in each article, and therefore a low chance that a random word is in a specific article. For these reasons, word term-frequencies are stored in a ‘dictionary’, which is a python hashmap. 

  *	Correspondingly, the master Tf-Idf matrix is stored in a sparse ‘Dictionary of keys’.    This has the added advantage of allowing matrices to be built incrementally. **Figure 3** gives a visualization of the Tf-Idf matrix and demonstrates the low density. The amount of non-zeros is approximately .10-.15 percent; a sparse matrix far more efficient.  It terms of memory, a dense Tf-Idf matrix representation of all articles takes a little over 4GB, whereas the sparse representation takes just 6.5MB, less than 1/600th the space. This shows a good correlation with the density of the matrix, such that the ratio of sizes is ~ the density. This supports scalability, as more articles will introduce more words and maintain a low density. 

  *	Although the size of the current dataset is known, the future dataset’s size is unknown and could be orders of magnitude larger than the current dataset which is under 30MB. The program was crafted with this in mind, and at no point need to hold more than a subset of the data in memory. Even within the only portion that must contain data from all sources, (the matrices), representations are numerical and as such scale much better than their textual counterparts. (30MB vs 6MB)

  *	To minimize the size of the data written to long term storage, only a critical subset of the matrices are written to storage. The non-included matrices are then derived from the stored matrices. 

# Issues Addressed

1.	Datasets
    *	Difficulty finding and obtaining a relevant dataset, needed something applicable to news domain, with significant data per entry and pre-existing relationships
        2.	Newsspace - http://www.di.unipi.it/~gulli/AG_corpus_of_news_articles.html
            *	1 million news articles from 2000 new sources
            *	Descriptions and titles, data preserved by link, many of which are dead. Therefore limited substance beyond titles 
        2.	Stanford Snap Database – http://snap.stanford.edu/data
            *	Includes large repository of various networks (Social, web, purchasing etc)
            *	However no political or news domain – closest is patents
        2.	GDelt Project Database - http://gdeltproject.org/data.html#documentation
            *	Event database – Events from 1979 to 2014 
                * Good data, but article links and minimal text to operate over
            *	Global Knowledge Graph – Similar but includes grouping and subset info (like organizations and locations). Excellent source of tags
        2.	MSR Paraphrase Corpus - http://research.microsoft.com/en-us/downloads/607d14d9-20cd-47e3-85bc-a2f65cd28042/
            *	5800 pairs of sentences extracted from news sources, no more than 1 per article, with human annotations(1/0) if they are equivalent
            *	Good as from news sources, however lacks substances, and sentences are relatively disjoint, reducing underlying relationships
        2.	Reuters21578 - https://archive.ics.uci.edu/ml/datasets/Reuters-21578+Text+Categorization+Collection
            *	21K articles from different news sources in 1979, including title, author and full text
            *	Includes some tagging on topics
            *	Chosen for completeness, size, and inclusion of tags

1.	Reuters Dataset
    2.	Found after newsspace corpus, meaning  restructuring of all data handling, also no more Minidom or XML Tree

    2.	Large amount of project time used to sift and improve filtering of the dataset

    2.	Used ‘body’ tag incorrectly, which cannot be indexed in .sgm parsing, as it is a keyword for the entire file’s contents

    2.	Inconsistent use of tags, some articles has no contents, and even worse it is often replaced with junk (ex: ‘Blah blah blah’). As a solution, filtering 21K articles to 13K only operating on those with topics, which by observation are cleaner.

1.	Python

    2.	First experience with Python; new libraries and capabilities. For example, after switching to the reuters corpus I discovered packages for parsing and handling the newsspace data.
    2.	Very flakey documentation, often reliant on user posts to compare implementations
    2.	Python is unable to pickle (write) sparse matrices, and so must write in Matlab format. Furthermore this means that all matrices must be destroyed before the object can be written.

# Results

1.	Developed system capabilities:

    *	Reformatting of input dataset to parse-able format, correcting structural errors and removing useless articles. Rewrites cleaned dataset separately, optionally in pretty printed format for human reading.
 
    *	Filtering of dataset of stop-words, tokenization and hash-storage of word and usages.
 
    *	Construction of Tf-Idf sparse matrix, logarithmic normalization, and sparse singular value decomposition into K highest value features. [Tf-Idf = U * S * Vt]. Visualization of features values (**Features 1 & 2**) can confirm a choice of dimensionality.

    *	Use of K-Means Clustering to group articles. Although articles are tagged with topics, tagging is a bit inconsistent, and K-means clustering allows unsupervised classification, allowing for expansion of the article domain. This can be implemented on either article or words. **Figures 4 & 5** demonstrate implementation of solving for the number of clusters using the ‘Elbow method’. This has the advantage of allowing a tradeoff between the number of clusters and cluster distortion.

    *	High dimensional to low dimensional queries of terms, phrases or sentences, with especially good accuracy on concise queries. Also does linguistic derivations on mismatched queries using calls to Python’s NLTK library; supporting singulars, plurals, incorrect spelling and similar words.

    *	Backup and restore of entire database, including the extracted article info, matrices, clusters and base object info.

2.	Observations

    *	**Figures 4 and 5** demonstrate the difference in return per number of clusters in articles and words respectively. The shape of the article clusters is more ideal, with a pronounced exponential curve and a distinct elbow. This reinforces that K-means clustering was an effective method for partitioning the articles into groups. Based on the graph, an optimal amount of article clusters is around 500-700, and 400-600 being approximately optimal for the number of word clusters.

    *	Observation of **Figures 1 & 2** show an exponential curve of derived feature values. The exponential curves indicate that there are a few very decisive features of the dataset. This supports the efficacy of using dimensionality reduction, as we can capture those top features. **Figures 1 & 2** demonstrate that this dimensionality can be as low as 50, while still maintaining a good representation. **Figures 6 & 7** provide an alternate visualization of the feature values with 500 features included. Studies have shown that this feature dimensionality is expected for LSA, and this the power of the dimensionality reduction is enough to represent corpora of millions of articles with 300+- 50 terms.

    *	Small concentrated queries yield better results than large imprecise queries, due to the variation introduced by loose word choice. 

    *	Article clustering is highly effective, and returns articles that are similar to query in context or subject, without necessarily explicitly including a direct reference. This is critical in the domain of news articles, where internal and inter-article relationships are numerous but loosely correlated.

    *	Word Clustering is semi-effective, grouping related words and even numbers of the same order. The results are imprecise however, and clusters often include extraneous terms. This is relevant because these extraneous terms are actually from internal relationships that have appeared within the dataset. We can use this to our advantage, by partitioning our data initially, and then determining what unexpected relationships may develop. With more data, these work clusters can be expected to filter these smaller relationships in favor of the more general semantic relationships between words.

# Looking forward / Improvements 

* One inescapable disadvantage to LSA is that ultimately the features that are derived are unlabeled, and much like the internal values of a neural network, and difficult to interpret or understand beyond their numerical representations. However, this also opens up the possibility of expanding the number of features arbitrarily, by adding additional objects besides words into the matrix, like n-grams, or the results of classifiers. 

* One of the weaknesses of LSA is its ‘bag of words’ type model, where word ordering is ignored in favor of context. To recover valuable word orderings, a second or conjoined matrix of n-grams can be reduced in dimension to determine features relating to n-grams, which can now be used alongside the original extracted data for classification and queries.

* Currently word derivations (spelling correction, synsets, etc) is only performed on a query mismatch. It would help, especially in cases of low original dimensionality, to include likely derivations into the Tf-Idf matrix. It would be important to mark these as different than the original term to avoid corruption of internal semantic relationships.

* The reduction of the Tf-Idf matrix is current done via a call to Scipy’s Sparse-SVD method. This is by far the most process intensive portion of the algorithm taking about 3 minutes for 50 dimensions, 5 for 100, and 15-20 for 500 dimensions. This computation can be done in parallel, which would greatly reduce the time required. My original efforts with PyCuda had been unsuccessful, however I recently discovered that Continuum (The makers of Anaconda), offer a student license that support parallel computation. This would be an ideal venue to pursue parallelization.  


# Figures
 

![SVD_50, S.png](https://bitbucket.org/repo/MAR8X8/images/514753971-SVD_50,%20S.png)

Figure 1. Nth most valuable feature of 50 dimensional decompositon 

![SVD_500, S.png](https://bitbucket.org/repo/MAR8X8/images/4184420164-SVD_500,%20S.png) 

Figure 2. Nth most valuable feature of 500 dimensional decompositon 

![TF-IDF Matrix.png](https://bitbucket.org/repo/MAR8X8/images/183351570-TF-IDF%20Matrix.png)

Figure 3. Sparse TF-Idf. Note the striped rows and columns, indicating high value word

![Elbow Method SVD 500, onlyTopics Optimal 500-1000, best about 600.png](https://bitbucket.org/repo/MAR8X8/images/2154765461-Elbow%20Method%20SVD%20500,%20onlyTopics%20Optimal%20500-1000,%20best%20about%20600.png)

Figure 4 Elbow Method for Determining best K for Articles

![Elbow Method SVD 500, onlyTopics WORDS.png](https://bitbucket.org/repo/MAR8X8/images/2688442636-Elbow%20Method%20SVD%20500,%20onlyTopics%20WORDS.png)

Figure 5 Elbow Method for Determining best K for Words 

![SVD_500, S AnyTopics.png](https://bitbucket.org/repo/MAR8X8/images/1844710907-SVD_500,%20S%20AnyTopics.png)

Figure 6. Plot of feature values with 500 dimensions.  

![Histogram of S, K=500, 500Bins.png](https://bitbucket.org/repo/MAR8X8/images/3870948235-Histogram%20of%20S,%20K=500,%20500Bins.png)

Figure 7. Histogram of Feature values in 500 Dimensions with 500 bins 

![Words articleCount vs totalCount for all corpus with filtering.png](https://bitbucket.org/repo/MAR8X8/images/2698138019-Words%20articleCount%20vs%20totalCount%20for%20all%20corpus%20with%20filtering.png)

Figure 8. # of article appearances vs # of total appearances per word