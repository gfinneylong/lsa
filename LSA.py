import math
import glob
import ntpath
import os
import time
import sys

__author__ = 'gio, gfinneylong@gmail.com, bitbucket.org/gfinneylong'

#from gensim import corpora, models
from xml.dom import minidom
import xml.etree.ElementTree as ElemTree
import random as rand
import pandas as pd
from scipy.sparse import coo_matrix
import matplotlib.image as mpimg
from PIL import Image
from pandas.tools.plotting import scatter_matrix
#from scipy.sparse.linalg import svds
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import cosine

from bs4 import BeautifulSoup, SoupStrainer
from numpy import zeros
import numpy as np
from textblob import TextBlob
import re
import matplotlib.pylab as plt
import matplotlib.cm as cm
import matplotlib
from scipy.linalg import svd, inv
from math import log
from scipy.sparse import linalg as spsi, dok_matrix
from scipy.sparse import dok_matrix
from sklearn.metrics.pairwise import linear_kernel
from scipy.cluster.vq import vq, kmeans, whiten, kmeans2
import scipy.io as sio
from colorama import init
from colorama import Fore, Back, Style
#from termcolor import colored
import code
import readline
import rlcompleter
import collections
from collections import OrderedDict
import pickle # Note uses cPickle automatically ONLY IF python 3


reuters_base_dir = 'reuters21578/'
reutersClean_base_dir = 'reutersClean/'



    #NOTE README
    #Use the regex ^<UNKNOWN>.*?</UNKNOWN>. to do filtering of reuters xml of UNKNOWN in np++
    #Then use What is inbetween the @'s here @ TOPICS=.*?NEWID=".*?"@
    #Finish with inbetween @'s here @&#.;@
    #Manually added <ROOT> and </ROOT> at second and last lines

class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("logfile.log", "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

#TODO make these self methods.
def getAllReutersCorpus(baseDir):
    """
    Grabs returns a list of all files in a base directory that start with reut2 and end with .sgn
    :param baseDir:
    :return:
    """
    AllCorps = glob.glob(baseDir + "reut2*.sgm")
    return AllCorps

def cleanReutersCorpus(corpus, cleanBaseDir):
    """
    Cleans a single corpus, and rewrites the clean version to the supplied cleanBaseDir, using the original filename
    This cleaning is specifically to remove some of the strange formatting that is contained in the reuters docs.
    :param corpus: The full/relative path and filename of the single corpus to be cleaned
    :param cleanBaseDir: The full/relative path to the folder to which cleaned the clean corpus is written
    :return:
    """
    dirtyIn = open(corpus, 'r')
    corpusCleaned = cleanBaseDir + os.path.basename(corpus)
    cleanOut = open(corpusCleaned, 'w')
    #help from http://stackoverflow.com/questions/10525867/extracting-tag-attributes-with-beautifulsoup
    delUnknown = 0 # This is going to be used to delete the <unknown> tag
    for line in dirtyIn:
        if not delUnknown:
            if(re.search('<UNKNOWN>',line)):
                delUnknown = 1
                # DO NOT WRITE THE LINE! It's evil!
            else:
                # continue on filtering, now that we know that we're no in an unknown block
                lineBuf1 = re.sub("<BODY>", "<TEXTBODY>", line)
                #Technically this and the below can be done together with BODY>, but this should be more accurate
                lineBuf2 = re.sub("</BODY>", "</TEXTBODY>",lineBuf1)
                lineBuf3 = re.sub("&#[0-9]+;","", lineBuf2)
                # if not lineBuf3 == line:
                    #print('Changed a line from:\n' + line + 'To:\n' + lineBuf3)
                # Done filtering, write back the cleaned data
                cleanOut.write(lineBuf3)
        elif re.search(r'</UNKNOWN>', line): #the r infront makes it a raw string
                delUnknown = 0
    dirtyIn.close()
    cleanOut.close()

def filterReutersCorpus(corpus, doPrettify, onlyWithTopics):
    """
    Filters a single corpus and writes OVER the input file. Returns articles filtered in corpus'
    :param corpus: The full/relative path to the corpus file
    :param doPrettify: True/False, True => When rewriting the contents, prettify them to make them easier to read
    :param onlyWithTopics: True/False, True => Only rewrite documents that have a topic listed.
        Useful for reviewing accuracy directly against the topics, rather than against untagged documents
    :return: The number of documents that did not have an article body.
        This is only useful for reviewing the corpus docs.
    """
    assert doPrettify in [True, False, 1, 0]
    assert onlyWithTopics in [True, False, 1, 0]
    f = open(corpus, 'r+')
    corpusData = f.read()
    soup = BeautifulSoup(corpusData)
    possibleArticles = soup.findAll('reuters', recursive=True)
    fakeTexts = 0
    for article in possibleArticles:
        artTextBody = article.textbody
        if not artTextBody:
            #print('Article had a text but no body')
            fakeTexts +=1
            article.extract()
        elif onlyWithTopics:
            # May need to have topics..
            topics = article.topics
            if not (topics and topics.get_text()):
                #print('Article had a text and body but not topics')
                fakeTexts += 1
                article.extract()
    if not fakeTexts == 0:
        # going to overwrite the original file with the new filtered one
        f.seek(0) #go to start of file
        if not doPrettify:
            f.write(str(soup))
        else:
            f.write(soup.prettify())
        f.truncate() # Remove the trailing data left
    f.close()
    return fakeTexts

def cleanReutersCorpora(dirtyCorpusDir, cleanCorpusDir):
    """
    Cleans all corpora that can be found in the dirtyCorpusDir, and rewrites the clean ones to the cleanCorpusDir.
    Cleaning is used to remove unusual characters and mend inconvenient formatting.
    :param dirtyCorpusDir: The full/relative folder which contains the uncleaned corpora.
    :param cleanCorpusDir: The full/relative folder which will contain the cleaned corpora.
    :return:
    """
    dirtyCorpusList = getAllReutersCorpus(dirtyCorpusDir)
    for (i, dirtyCorp) in enumerate(dirtyCorpusList):
        print('Cleaning #' + str(i) + ': ' + dirtyCorp + ' rewriting as: ' + cleanCorpusDir + os.path.basename(dirtyCorp))
        cleanReutersCorpus(dirtyCorp, cleanCorpusDir)

def filterReutersCorpora(workingDir, doPrettify, onlyWithTopics,):
    """
    Filters all corpora from workingDir, and overwrites each unfiltered corpus with it's filtered version
    Filtering is used to modify the contents of a corpus by removing unwanted documents, and then rewriting.
    :param workingDir: The full/relative folder which contains the unfiltered corpora, and where we will overwrite said
        corpora with their newly filtered equivalents.
    :param doPrettify: True/False, True => When rewriting the contents, prettify them to make them easier to read
    :param onlyWithTopics: True/False, True => Only rewrite documents that have a topic listed.
    :return:
    """
    print("Filtering all corpora articles, prettify:" + str(doPrettify) + ' onlyTopics:' + str(onlyWithTopics))
    corpusList = getAllReutersCorpus(workingDir)
    fakeCounts = []
    for (corpusNum, corpus) in enumerate(corpusList):
        print('Filtering useless articles from corpus #' + str(corpusNum))
        fakeCounts.append(filterReutersCorpus(corpus, doPrettify, onlyWithTopics))
        # printing out what we are ignoring nicely formatted
    width = 4 # This controls the number of characters that can be used by any given string-value printed below
    print('Filtered the following number of articles from the respective corpora')
    if not max(fakeCounts) == 0:
        for corpNum in range(len(corpusList)):
            print(str(corpNum).ljust(width), end="")
        print('')
        for fake in fakeCounts:
            print(str(fake).ljust(width), end="")
        print('')
            # print ('\t'.join(fakeCounts[p:p+4]))
    else:
        print('\n* There were no articles removed from any of the corpora; they were already filtered')
        print('* This step can be ignored next time for speedup of runtime')

class WordInfo:
    """
    This stores the relevant info of a word from the corpora.
    """

    def __init__(self, word):
        """
        :param word: The string of the word
        articleCount: The number of articles that the word appears in (Used in TF-IDF)
        tfMap: A map from article number to the frequency of the word in that article
        totalFrequency: The total number of appearances accross all articles, = sum(tfMap)
        """
        self.word = word
        self.articleCount = 0
        self.tfMap = {}
        self.totalFrequency = 0

    def printInfo(self):
        print('Word: ' + str(self.word).ljust(12) + ', InArticles: ' + str(self.articleCount).ljust(3) + ', Total Occur: ' + str(self.totalFrequency))

class ReutersInfo:
    """
    This stores the relevant info extracted from an element of the reuters dataset.
    """

    def __init__(self, articleNum, title, author, body, topics, date, places):
        """
        artNum: The unique number/id used to identify the article.
        title: The title of the article as a string
        author: The authors name, as supplied in the article, as a string
        body: The contents of the article, as a string
        topics: A list of the supplied topics given with the article
        date: The date as referenced in the article, as a string
        places: A list of the supplied places given with the article
        hasTopics: True/False, True => The article has been pre-tagged with it's topic(s)
        topicList: The list of topics supplied with the article
        """
        self.artNum = articleNum
        self.title = title
        self.author = author
        self.body = body
        self.topics = topics #NOTE this is still unformatted; use topicList
        self.date = date
        self.places = places
        self.hasTopics = False
        self.topicList = []
        if self.topics and self.topics.get_text():
            self.hasTopics = True
            for topic in self.topics.find_all('d'):
                self.topicList.append(topic.get_text())
            #print('Found topics for RI: ' + str(self.topicList))


    def printInfo(self):
        """
        Prints all useful information about an article, including the body
        This function will have a large output, due to printing the body
        """
        print('Art#:' + str(self.artNum) + ', Title:' + str(self.title.get_text()) + ' , Topics: ' + str(self.topics) + ', Date:' + str(self.date) + ', Places:' + str(self.places)+ ' , Author:' + str(self.author) + ', Body:' + str(self.body))

    def printInfoSmall(self):
        """
        Prints useful information about an article, excluding non-supplied info and the body to save output space
        """
        if self.hasTopics:
            print('Art#:' + str(self.artNum).ljust(5) + ', Title:' + str(self.title.get_text()).ljust(55) + ' , Topics: ' + str(self.topicList).ljust(25) + ', Date:' + str(self.date) + ', Places:' + str(self.places)+ ' , Author:' + str(self.author))
        else:
            if self.title:
                print('Art#:' + str(self.artNum).ljust(5) + ', Title:' + str(self.title.get_text()).ljust(55) + ' , No Topics' + ', Date:' + str(self.date) + ', Places:' + str(self.places) + ' , Author:' + str(self.author))
            else:
                print('Art#:' + str(self.artNum).ljust(5) + ', Title:' + str(self.title).ljust(55) + ' , No Topics' + ', Date:' + str(self.date) + ', Places:' + str(self.places) + ' , Author:' + str(self.author))


class ReutersDataset:
    """
    Stores all of the ReutersInfo that are extracted from a particular dataset
    """
    def __init__(self, dirtyCorpusDir, cleanCorpusDir, make0read1makeW2, svdk):
        """
        dirtyCorpusDir: The full/relative folder which contains the uncleaned corpora.
        cleanCorpusDir: The full/relative folder which will contain the cleaned corpora.
        make0read1makeW2:
            0 => Make the dataset ONLY
            1 => Read the dataset from a previous write ONLY, this is a good way to avoid reprocessing data
            2 => Make then write the dataset; required to later read the dataset
        svdk: The dimensionality of the singular value decomposition, ie how many features to reduce to
        """
        assert make0read1makeW2 in [0, 1, 2]

        self.start_time = time.time()
        self.isSparse = True
        self.numAC = 600 # Number of article clusters to generate, 600 optimal for entire dataset
        self.numWC = 700 # Number of word clusters to generate
        self.svdDim = svdk
        self.dataFile = str(svdk) + 'K_lsaData.pickle'
        self.matrixFile = str(svdk) + 'K_LSA_matrices'

        doCosSim = 0 #25   # NOTE that the value this is set to will be the top n found. 0 disables
        doCluster = 0 # NOTE that this value is a toggle controlled within the source code whether to use
                      # NOTE K-Means clustering to group the articles AND words into their respective clusters
        if make0read1makeW2 == 0: # 0 => Make only
            self.makeSelf(dirtyCorpusDir, cleanCorpusDir)
        elif make0read1makeW2 == 1: # 1 => Read only
            self.readSelf()
            self.readClusters() # Now includes setting
            self.printTime()
        elif make0read1makeW2 == 2: # 2 => Make then write the result
            # self.printTime()
            self.makeSelf(dirtyCorpusDir, cleanCorpusDir)
            self.writeSelf()
        self.cossimNoSVD(doCosSim) # Note; Optional, this attempts to find the most similar words across all corpora
        if doCluster:
            self.getArticleClusters(self.numAC)
            self.printTime()
            self.setArticleClusters() # Take any advantage to set article clusters
            self.printTime()
            self.getWordClusters(self.numWC)
            self.printTime()
            self.setWordClusters()
            self.printTime()
            self.writeClusters()


    def pac(self, c): # Print-Article-Cluster(c)
        """
        pac = Print Article Cluster
        Mini-method for printing an article cluster
        :param c: The article cluster id to print
        """
        printArticleListSmall(self.clusters[c])


    def pwc(self, c): # Print-Word-Cluster(c)
        """
        pwc = Print Word Cluster
        Mini-method for printing a word cluster
        :param c: The word cluster id to print
        """
        printWordList(self.clustersW[c])


    def q(self, string, count):
        """
        q = Query Articles
        Mini method for querying a string against the articles
        :param string: The string which is searched against the articles
        :param count: The number of articles to return
        """
        self.query(string, count)


    def printACSizes(self):
        """
        Prints the size of each article cluster
        """
        print('Article Cluster # :', end = '')
        for i in range(len(self.clusters)):
            print(str(i).rjust(4), end=', ')
        print('\n        # Articles:', end = '')
        for i in range(len(self.clusters)):
            print(str(len(self.clusters[i])).rjust(4), end=', ')
        print('')

    def printWCSizes(self):
        """
        Prints the size of each word cluster
        """
        print('Word Cluster # :', end = '')
        for i in range(len(self.clustersW)):
            print(str(i).rjust(5), end=', ')
        print('\n        # Words:', end = '')
        for i in range(len(self.clustersW)):
            print(str(len(self.clustersW[i])).rjust(5), end=', ')
        print('')

    def writeClusters(self):
        """
        Writes both the article and word clusters to a pickle file. Will fail if missing either
        Also note that this writes the lists not the clusters themselves, which would be terrible storage usage
        """
        print('Writing both sets of cluster maps (Not the clusters themselves)')
        if self.onlyWithTopics:
            t = '_Topics.pickle'
        else:
            t = '_AnyTopic.pickle'
        pre = str(self.svdDim) + 'K_'
        filename = pre + str(self.numAC) + 'AC_' + str(self.numWC) + 'WC' + t
        pack = [self.bookW, self.distortionW, self.centLabelsW, self.centroidsW, self.least_common_listW, self.book, self.distortion, self.centLabels, self.centroids, self.least_common_list]
        pickle.dump(pack, open(filename, "wb"))


    def readClusters(self):
        """
        Reads both article and word clusters from a pre-set pickle file
        """
        # NOTE WARNING no safeguard yet on this method!
        # Must reconstruct the clusters after reading, for storage efficiency
        print('Loading and reconstructing both clusters;', end='')
        if self.onlyWithTopics:
            t = '_Topics.pickle'
        else:
            t = '_AnyTopic.pickle'
        pre = str(self.svdDim) + 'K_'
        filename = pre + str(self.numAC) + 'AC_' + str(self.numWC) + 'WC' + t
        print(' Dictionaries from file: ' + str(filename), end='\n')
        pack = pickle.load(open(filename, 'rb'))
        self.bookW = pack[0]
        self.distortionW = pack[1]
        self.centLabelsW = pack[2]
        self.centroidsW = pack[3]
        self.least_common_listW = pack[4]
        self.book = pack[5]
        self.distortion = pack[6]
        self.centLabels = pack[7]
        self.centroids = pack[8]
        self.least_common_list = pack[9]
        # TODO map would be safer.
        self.setArticleClusters()
        self.setWordClusters()

    def qac(self, string, count):
        """
        qac = Query Article Cluster
        Finds the count # of best article clusters for the search string
        :param string: The string which is searched against the articles
        :param count: The number of articles to return
        """
        print('\nQuerying: ' +  string)
        query_start_time = time.time()
        blob = TextBlob(string.lower()) # HACK making lower here, should be ok for now
        wordSet = set(blob.words)
        goodWords = 0
        for (wNum, word) in enumerate(wordSet):
            accum = [0] * self.svdDim
            if not word in self.wordDict:
                print('***New word:' + str(word) + ' not yet added, so trying derivatives')
                b = TextBlob(word)
                tryWords = [
                    b.words[0].singularize(),
                    b.words[0].pluralize(),
                    b.words[0].correct() # try fixing spelling!
                ]
                foundW = []
                wVals = []
                for word in tryWords:
                    if word in self.wordDict:
                        foundW.append(word)
                if len(foundW) == 0:
                    print('Couldn\'t match any derivations of word, Trying synsets:')
                    print(b.words[0].synsets[:5])
                    tryWords = []
                    localAcc = [0] * self.svdDim
                    foundSyn = 0.0
                    if(len(b.words[0].synsets[:5]) != 0):
                        for syn in b.words[0].synsets[:5]:
                            tryWords += syn.lemma_names()
                        for word in tryWords:
                            if word in self.wordDict:
                                foundSyn += 1
                                print('Matched a Synset word: ' + word)
                                ind = list(self.wordDict.keys()).index(word)
                                buf = self.Uo[ind, :]
                                localAcc = np.add(localAcc, buf)
                        localAcc = localAcc / foundSyn
                        accum = np.add(localAcc, accum)
                    else:
                        print('Couldn\'t find any appropriate syntsets')
                else:
                    goodWords += 1
                    print('Found ' + str(len(foundW)) + ' possible derivations')
                    max = 0
                    bestInd = 0
                    for (i,word) in enumerate(foundW):
                        ind = list(self.wordDict.keys()).index(word)
                        bufr = math.fabs(np.sum(self.U[ind, :])) #Using U instead of U because trying to find best wrod given features.
                        print('Word:' + word + ' ranking:' + str(bufr))
                        if bufr > max:
                            bestInd = ind
                    buf = self.Uo[bestInd,:]
                    accum = np.add(accum, buf)
            else:
                goodWords += 1
                ind = list(self.wordDict.keys()).index(word)
                buf = self.U[ind, :] # This should technically be Uo, which is then mult by S. Instead using U, which is Uo*S
                # print('W:' + word + ', Buf:' + str(buf))
                accum = np.add(accum, buf)
        clusterNums = []

        if goodWords != 0:
            self.test = accum
            sims = []
            for art in range(len(self.clusters)):
                sims.append(cosine_similarity(accum, self.clusterVals[art]))    # self.Vt[:,art]))# np.multiply(self.S, self.Vt[:,art])))
            #self.sortedSim = np.asarray(sims).argsort()[-len(sims):][::-1]
            self.sortedSims = reversed(sorted(range(len(sims)), key=sims.__getitem__)) # From: http://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python
            print('Time for query: %.3f' % (time.time() - query_start_time), end='')
            print(' seconds', end='\n')
            for (i, val) in enumerate(self.sortedSims):
                if i >= count:
                    break
                else:
                    print('\nIndex:' + str(i) + ', Similarity:' + str(sims[val]))
                    #foundArt = self.getReutersArticle(val)
                    self.printArticleCluster(val)
                    #foundArt.printInfoSmall()
                    clusterNums.append(val)
            return clusterNums
        else:
            print('No words in the search matched any derivations; try a new search')
            return clusterNums

    def getWordClusters(self, k):
        """
        Stores the (label, #wordsWithLabel) pairs into self.least_common_list
        :param k: The number of clusters to group words into
        """
        print('Clustering words into ' + str(k) + ' clusters')
        self.kW = k
        (self.bookW, self.distortionW)  = kmeans(self.U, self.kW)
        (self.centLabelsW, self.centroidsW) = vq(self.U, self.bookW)
        self.least_common_listW = list(reversed(collections.Counter(self.centLabelsW).most_common()))

    def makeSelf(self, dirtyCorpusDir, cleanCorpusDir):
        """
        A part of init(), this is where the control are for generating a Reuters Dataset
        This method handles the calls to cleaning, filtering, and the singular value decomposition of the matrix
        :param dirtyCorpusDir: The full/relative folder which contains the uncleaned corpora.
        :param cleanCorpusDir: The full/relative folder which will contain the cleaned corpora.
        """
        debugOverride = 1 # HACK TOGGLE
        doClean = 1     # HACK TOGGLE
        doFilter = 1    # HACK TOGGLE
        doPrettify = 0  # HACK TOGGLE part of filtering
        self.onlyWithTopics = 1 # HACK TOGGLE part of filtering
        if not debugOverride:
            doCleanInput = input("Do the corpora need to be cleaned? (n/Any) ")
            doClean = not re.search('^[nN][Oo]?', doCleanInput)
            doFilterInput = input("Do the cleaned corpora need to be filtered of useless articles? (n/Any) ")
            doFilter = not re.search('^[nN][Oo]?', doFilterInput)
            if doFilter:
                doPrettifyInput = input("Pretty print the filtered corpora? (n/Any) ")
                doPrettify = not re.search('^[nN][Oo]?', doPrettifyInput)
                owtInput = input('Should articles without topics be filtered out? (n/Any) ')
        if doClean:
            cleanReutersCorpora(dirtyCorpusDir, cleanCorpusDir)
            self.printTime()
        if doFilter:
            filterReutersCorpora(cleanCorpusDir, doPrettify, self.onlyWithTopics)
            self.printTime()
        self.corpusList = getAllReutersCorpus(cleanCorpusDir)
        print('Final Corpora: ' + ", ".join(self.corpusList))

        # Time to load up data, now that it's fully prepped
        print("\nNow loading article data")
        self.printTime()
        self.loadArticleData()
        print('\nDone loading articles. Starting freq. matrix. [' + str(len(self.wordDict)) + ' words, ' + str(self.articleCount ) + ' articles]')
        print('Corpus Article Counts:' + str(self.corpusAC))
        self.generateMatrix()
        self.printTime()

        print('\nDoing SVD on matrix')
        self.doSVD(self.svdDim) # HACK
        print('Shape Of U,S,V : ' + str(self.U.shape) + ' ' + str(self.S.shape) + ' ' + str(self.V.shape))

        # HACK
        self.Uo = self.U
        #self.So = self.S
        self.Vto = self.Vt
        self.Vo = self.V
        self.U = self.U * self.S
        self.V = self.S * self.V
        self.Vt = self.V.transpose()
        # HACK


        # plt.scatter(list(self.V[len(self.V):]), list(self.V[(len(self.V)-1):]))
        #self.findBestArticleK(10, 3000, 50)
        # TODO save the distortion and use it to find the tightest matrices?
        # plotMatrixPair(self.dok_matrix, self.matrix)
        # plotHist(self.S, 100)
        # plotHist(self.docCosineSimilarity, 100)
        # plotHist(self.wordCosineSimilarity, 100)
        # plt.matshow(self.matrix)

    def writeSelf(self):
        """
        Writes a reuters dataset to a predetermined pickle file
        Requires the temporary caching of the matrices, as pickle cannot handle sparse matrices
        """
        # save the dok_matrices because they can't be written...
        # Then del, dump, then restore
        print('Writing matrices and dictionaries to: ' + str(self.dataFile))
        writeDic = { #'U'  : self.U,
                     'Uo' : self.Uo,
                     'S'  : self.S,
                     #'V'  : self.V,
                     'Vo' : self.Vo,
                     #'Vt' : self.Vt,
                     #'Vto' : self.Vto,
                     'dok': self.dok_matrix
                     }
        sio.savemat(self.matrixFile, writeDic)

        # interwoven buffering and deleting for optimal memory efficiency
        bU = self.U
        del self.U
        bUo = self.Uo
        del self.Uo
        bS = self.S
        del self.S
        bV = self.V
        del self.V
        bVo = self.Vo
        del self.Vo
        bVt = self.Vt
        del self.Vt
        bVto = self.Vto
        del self.Vto
        bdok = self.dok_matrix
        del self.dok_matrix
        # del self.bookW
        # del self.distortionW
        # del self.centLabelsW
        # del self.centroidsW
        # del self.least_common_listW
        # del self.book
        # del self.distortion
        # del self.centLabels
        # del self.centroids
        # del self.least_common_list
        pickle.dump(self.__dict__, open(self.dataFile, "wb")) # Writing self: http://stackoverflow.com/questions/2709800/how-to-pickle-yourself
        self.U = bU
        self.Uo = bUo
        self.S = bS
        self.V = bV
        self.Vo = bVo
        self.Vt = bVt
        self.Vto = bVto
        self.dok_matrix = bdok

    def readSelf(self):
        """
        Reads a reuters dataset from a prespecified dataFile.
        Regenerates the matrices that were deleted/not written to save space
        """
        print('Reading matrices and dictionaries from: ' + str(self.dataFile))
        tempDict = pickle.load(open(self.dataFile, "rb"))
        self.__dict__.update(tempDict)
        self.loaded = sio.loadmat(self.matrixFile)
        self.Uo  = self.loaded['Uo']
        self.S   = self.loaded['S'].flatten() # Really annoying, makes a (50,1) matrix instead of (50,)
        self.Vo  = self.loaded['Vo']
        self.U = self.Uo * self.S
        self.V = self.S * self.Vo
        self.Vto = self.Vo.transpose()
        self.Vt = self.V.transpose()
        self.dok_matrix = self.loaded['dok']
        del self.loaded
        print('Successfully loaded matrices, #Articles:' + str(self.articleCount) + ', #Unique Words:' + str(len(self.wordDict)) + ', SVD Matrices contain top ' + str(self.svdDim) + ' features')
        self.start_time = time.time()

    def query(self, string, count):
        """
        Queries a string against the article within the dataset, and returns the count best matches.
        :param string: The search string to query against the article
        :param count: The number of articles to return
        :return: Returns a list of ReutersInfo, corresponding to the best matching articles
        """
        print('\nQuerying: ' +  string)
        query_start_time = time.time()
        blob = TextBlob(string.lower()) # HACK making lower here, should be ok for now
        wordSet = set(blob.words)
        goodWords = 0

        for (wNum, word) in enumerate(wordSet):
            accum = [0] * self.svdDim
            #print('W:' + str(word))
            #print("Found word at index:" + str(ind))
            #buf = self.wordDict[word]  # buf is a wordInfo,
            #print('Len wd:' + str(len(self.wordDict)))
            #print(self.U.shape)
            if not word in self.wordDict:
                print('***New word:' + str(word) + ' not yet added, so trying derivatives')
                b = TextBlob(word)
                tryWords = [
                    b.words[0].singularize(),
                    b.words[0].pluralize(),
                    b.words[0].correct() # try fixing spelling!
                ]
                foundW = []
                wVals = []
                for word in tryWords:
                    if word in self.wordDict:
                        foundW.append(word)
                if len(foundW) == 0:
                    print('Couldn\'t match any derivations of word, Trying synsets:')

                    print(b.words[0].synsets[:5])
                    tryWords = []
                    localAcc = [0] * self.svdDim
                    foundSyn = 0.0
                    if(len(b.words[0].synsets[:5]) != 0):
                        for syn in b.words[0].synsets[:5]:
                            tryWords += syn.lemma_names()
                        for word in tryWords:
                            if word in self.wordDict:
                                foundSyn += 1
                                print('Matched a Synset word: ' + word)
                                ind = list(self.wordDict.keys()).index(word)
                                buf = self.Uo[ind, :]
                                localAcc = np.add(localAcc, buf)
                        localAcc = localAcc / foundSyn
                        accum = np.add(localAcc, accum)
                    else:
                        print('Couldn\'t find any appropriate syntsets')

                else:
                    goodWords += 1
                    print('Found ' + str(len(foundW)) + ' possible derivations')
                    max = 0
                    bestInd = 0
                    for (i,word) in enumerate(foundW):
                        ind = list(self.wordDict.keys()).index(word)
                        bufr = math.fabs(np.sum(self.U[ind, :])) #Using U instead of U because trying to find best wrod given features.
                        print('Word:' + word + ' ranking:' + str(bufr))
                        if bufr > max:
                            bestInd = ind
                    buf = self.Uo[bestInd,:]
                    accum = np.add(accum, buf)

            else:
                goodWords += 1
                ind = list(self.wordDict.keys()).index(word)
                buf = self.U[ind, :] # This should technically be Uo, which is then mult by S. Instead using U, which is Uo*S
                # print('W:' + word + ', Buf:' + str(buf))
                accum = np.add(accum, buf)

        articleInfos = []
        if goodWords != 0:
            self.test = accum
            sims = []
            for art in range(self.articleCount):
                sims.append(cosine_similarity(accum, self.Vt[:,art]))# np.multiply(self.S, self.Vt[:,art])))
            #self.sortedSim = np.asarray(sims).argsort()[-len(sims):][::-1]
            self.sortedSims = reversed(sorted(range(len(sims)), key=sims.__getitem__)) # From: http://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python
            print('Time for query: %.3f' % (time.time() - query_start_time), end='')
            print(' seconds', end='\n')

            for (i, val) in enumerate(self.sortedSims):
                if i >= count:
                    break
                else:
                    print('\nIndex:' + str(i) + ', Similarity:' + str(sims[val]))
                    foundArt = self.getReutersArticle(val)
                    foundArt.printInfoSmall()
                    articleInfos.append(foundArt)
            return articleInfos
        else:
            print('No words in the search matched any derivations; try a new search')
            return articleInfos

    def setArticleClusters(self):
        """
        Takes the data saved in least_common_list and stores the articleClusterInfo.
        This is not done originally; its much more memory intensive
        """
        self.clusters = [list() for _ in range(len(self.least_common_list))]
        self.clusterVals = [[0] * self.svdDim for _ in range(len(self.least_common_list))]
        # self.clusters = [[]] * len(self.least_common_list)
        print('Loading cluster data from cleaned files')
        cur_corpus_index = 0
        modulus = 0
        print('Opening ' + str(self.corpusList[0]), end=', ')
        f = open(self.corpusList[cur_corpus_index], 'r+')
        corpusData = f.read()
        soup = BeautifulSoup(corpusData)
        possibleArticles = soup.findAll('reuters', recursive=True)
        for (artnum, label) in enumerate(self.centLabels):
            # print("artnum: " + str(artnum))
            if artnum - modulus >= self.corpusAC[cur_corpus_index]:
                f.close()
                modulus += self.corpusAC[cur_corpus_index]
                cur_corpus_index += 1
                if cur_corpus_index == len(self.corpusList):
                    print('About to fail ' + str(artnum))
                print(str(self.corpusList[cur_corpus_index]), end=', ')
                f = open(self.corpusList[cur_corpus_index], 'r+')
                corpusData = f.read()
                soup = BeautifulSoup(corpusData)
                possibleArticles = soup.findAll('reuters', recursive=True)
            # remain = remain % len(possibleArticles) # HACK
            # print(str(artnum - modulus))
            article = possibleArticles[artnum - modulus]
            self.clusters[label].append(ReutersInfo(artnum, article.title, article.author, article.textbody, article.topics, article.date, article.places))
            self.clusterVals[label] += self.Vo[artnum, :] # TODO switch to V :,art
        print('', end='\n')
        self.printACSizes()

        # for c in range(self.numAC):
        #    accum = [0] * self.svdDim
        #    for a in range(self.clusters[c]):
        #         accum += self.V[self.clusters[c][a].artnum]


    def setWordClusters(self):
        """
        Takes the data saves in least_common_listW and stores the wordInfo
        :return:
        """
        self.clustersW = [list() for _ in range(len(self.least_common_listW))]
        self.clusterWVals = [[0] * self.svdDim for _ in range(len(self.least_common_listW))]
        print('Loading word cluster data from dictionary')
        tempList = list(self.wordDict.items())
        for(wordnum, label) in enumerate(self.centLabelsW):
            (word, wInf) = tempList[wordnum]
            self.clustersW[label].append(wInf)
            self.clusterWVals[label] += self.Uo[wordnum, :] # TODO switch to U
        self.printWCSizes()


    def runShell(self):
        """
        Runs an interactive shell, that has access to the variables created so far and can create more.
        Used for debugging.
        :return:
        """
        vars = globals()
        vars.update(locals())
        readline.set_completer(rlcompleter.Completer(vars).complete)
        readline.parse_and_bind("tab: complete")
        shell = code.InteractiveConsole(vars)
        shell.interact()

    def loadArticleData(self):
        """
        Reads the supplied articles, and adds their words to self.wordDict while keeping track of their frequency etc
        :return:
        """
        self.wordDict = OrderedDict()
        # Making a dictionary, which maps a key(word) to an entry (WordInfo)
        articleNum = 0
        #TODO note that articleNum is going to be used in a dict map for each word

        self.articleWC = [] # word counts of each article #TODO Try storing unique words, vs word counts and see how that effects outcome
        self.corpusAC = [] # The number of articles per corpus

        for (corpusNum, corpus) in enumerate(self.corpusList):
            file = open(corpus, 'r+')
            corpusData = file.read()
            soup = BeautifulSoup(corpusData)
            possibleArticles = soup.findAll('reuters', recursive=True)
            print('Processing corpus #' + str(corpusNum) + ' which has ' + str(len(possibleArticles)) + ' articles')
            self.corpusAC.append(len(possibleArticles))
            for article in possibleArticles:
                #print(article.textbody.string)
                blob = TextBlob(article.textbody.string.lower())
                wordSet = set(blob.words)
                for word in wordSet:
                    if not word in self.wordDict: #has_key is depreciated
                        #Adding a new word!
                        self.wordDict.update({word : WordInfo(word)})
                    termFreq = blob.words.count(word, case_sensitive=False)
                    self.wordDict[word].tfMap.update({articleNum : termFreq})
                    self.wordDict[word].articleCount += 1
                    #TODO the above case sensitivity should circumvent case issues
                    self.wordDict[word].totalFrequency += termFreq
                articleNum += 1
                self.articleWC.append(len(blob.words))
        self.printDictShort()
        self.articleCount = articleNum
        #Now time to create a matrix :)

    def splotU(self):
        """
        Plots the U matrix of words vs word features
        :return:
        """
        plt.plot(self.U)
        plt.xlabel('Word Number')
        plt.ylabel('Word Features times Feature Values')
        plt.title('Self.U * Self.S')
        plt.tight_layout()
        plt.savefig('SVD_' + str(self.svdDim) + ', U.png', bbox_inches='tight')
        #plt.show()

    def splotS(self):
        """
        Plots the S matrix of feature values
        :return:
        """
        plt.plot(self.S)
        plt.xlabel('Nth Most valuable feature')
        plt.ylabel('Feature Value')
        plt.title('Feature Values')
        plt.tight_layout()
        plt.savefig('SVD_' + str(self.svdDim) + ', S.png', bbox_inches='tight')
        #plt.show()

    def splotV(self):
        """
        Plots the V matrix of articles vs article features
        :return:
        """
        plt.plot(self.S)
        plt.xlabel('Article Number')
        plt.ylabel('Article Features time Feature Values')
        plt.title('Self.S * Self.V')
        plt.tight_layout()
        plt.savefig('SVD_' + str(self.svdDim) + ', V.png', bbox_inches='tight')
        #plt.show()

    def findBestArticleK(self, min, max, step):
        """
        Tries a range of k values for clustering, then graphs the results against the total error.
        This allows use of the elbow method to find the best article K for a specific dataset.
        This process is time consuming, especially for large dataset.
        Together min, max, step are essentially the arguments of xrange()
        :param min: The minimum value of K to try
        :param max: The maximum value of K to try
        :param step: The step size of the K value as it ranges from min=>max
        :return:
        """
        'n is the max number to try of clusters, i is the += amount of n (step size)'
        print('\nUsing the elbow method to find optimal article cluster count\nRecord the elbow!')
        kVals = []
        distVals = []
        for z in range(math.floor((max - min) / step)):
            k = (z * step) + min
            self.getArticleClusters(k)
            kVals.append(k)
            distVals.append(self.distortion)
        plt.plot(kVals, distVals, marker='x')
        plt.grid(True)
        plt.xlabel('Number of Clusters (K)')
        plt.ylabel('Average cluster distortion')
        plt.title('Elbow method for K-Means Clustering on Articles\nManually Record the desired K value\nTotal Articles = ' + str(self.articleCount))
        plt.show()

    def findBestWordK(self, min, max, step):
        """
        Tries a range of k values for clustering, then graphs the results against the total error.
        This allows use of the elbow method to find the best word K for a specific dataset.
        This process is time consuming, especially for large dataset.
        Together min, max, step are essentially the arguments of xrange()
        :param min: The minimum value of K to try
        :param max: The maximum value of K to try
        :param step: The step size of the K value as it ranges from min=>max
        :return:
        """
        print('\nUsing the elbow method to find optimal word cluster count\nRecord the elbow!')
        kVals = []
        distVals = []
        for z in range(math.floor((max - min) / step)):
            k = (z * step) + min
            self.getWordClusters(k)
            kVals.append(k)
            distVals.append(self.distortionW)
        plt.plot(kVals, distVals, marker='x')
        plt.grid(True)
        plt.xlabel('Number of Clusters (K)')
        plt.ylabel('Average cluster distortion')
        plt.title('Elbow method for K-Means Clustering on Words\nManually Record the desired K value\nTotal Words = ' + str(len(self.wordDict)))
        plt.show()

    def getWordClusterInfo(self, n):
        """
        Compiles a list of wordInfo, each element corresponding to a word in a specific cluster
        :param n: The word cluster id from which the wordInfo of each word is compiled (nth smallest)
        :return:
        """
        n = n % len(self.least_common_listW)
        (matchLabel, matchVal) = self.least_common_listW[n]
        matchedWords = []
        tempList = list(self.wordDict.items())
        for (i,label) in enumerate(self.centLabelsW):
            if label == matchLabel:
                (word, wInf) = tempList[i]
                matchedWords.append(wInf)
        print('Found ' + str(len(matchedWords)) + ' words in cluster #' + str(n))
        return matchedWords

    def getArticleClusters(self, k):
        """
        Stores the (label, #articlesWithLabel) pairs into self.least_common_list by performing k-means clustering on the articles'
        :param k: The number of clusters which the articles are grouped into
        :return:
        """
        print('Clustering articles into ' + str(k) + ' clusters')
        self.k = k
        (self.book, self.distortion)  = kmeans(self.V, self.k)
        (self.centLabels, self.centroids) = vq(self.V, self.book)
        self.least_common_list = list(reversed(collections.Counter(self.centLabels).most_common()))

    def getReutersArticle(self, artNum):
        """
        Retruns the ReutersInfo for a specific article number
        :param artNum: The id of the article whose ReutersInfo we are retrieving
        :return: The ReutersInfo of the article
        """
        if artNum > self.articleCount:
            print('ERROR :: ARTNUM: ' + str(artNum) + ' ARTCOUNT: ' + str(self.articleCount))
        assert(artNum <= self.articleCount)
        remain = artNum
        for (i, corpus) in enumerate(self.corpusList):
            if remain <= self.corpusAC[i]:
                break
            else:
                remain -= self.corpusAC[i]
        f = open(self.corpusList[i], 'r+')
        corpusData = f.read()
        soup = BeautifulSoup(corpusData)
        possibleArticles = soup.findAll('reuters', recursive=True)
        remain = remain % len(possibleArticles) # HACK
        if remain >= len(possibleArticles):
            print('ERR: REMAIN: ' + str(remain) + ' len of posArt:' + str(len(possibleArticles)))
        article = possibleArticles[remain]
        info = ReutersInfo(artNum, article.title, article.author, article.textbody, article.topics, article.date, article.places)
        return info

    def getArticleClusterInfo(self, n):
        """
        Compiles a list of articleInfo, each element corresponding to a word in a specific cluster
        :param n: The article cluster id from which the wordInfo of each word is compiled (nth smallest)
        :return:
        """
        # now search for all labels that match leastLabel dcf .
        print('Clustering articles and saving article indices')
        n = n % len(self.least_common_list)
        (matchLabel, matchVal) = self.least_common_list[n]
        matchedArticlesNums = []
        for (i, label) in enumerate(self.centLabels):
            if label == matchLabel:
                matchedArticlesNums.append(i) #i is the article number
        print('Found ' + str(len(matchedArticlesNums)) + ' articles in cluster #' + str(n))
        matchedArticles = []
        for i in matchedArticlesNums:
            matchedArticles.append(self.getReutersArticle(i))
        return matchedArticles



    def cossimNoSVD(self, doCosSim):
        """
        Uses cosine-similarity measure to find the 'doCosSim'# of most similar words, on the matrix BEFORE svd
        :param doCosSim:
            0 => Do nothing
            >=1 => Find the doCosSim # of most similar words
        :return:
        """
        'This method is OPTIONAL, ie always run but will self exit dependent on value. Done to reduce size in exec'
        if doCosSim:
            print('\nCalculating Cosine Similarity over words WITHOUT using SVD reduction')
            if(self.isSparse):
                self.wordCosineSimilarity = linear_kernel(self.dok_matrix[0:1], self.dok_matrix).flatten() #GLOBAL SIM
                #self.wordCosineSimilarity = linear_kernel(self.U[0:1], self.U).flatten() #GLOBAL SIM
            else:
                #TODO if desired make a dense cosine similarity..
                #self.docCosineSimilarity = linear_kernel(self.dok_matrix[0:1], self.dok_matrix).flatten() #GLOBAL SIM
                print('***Cosine similarity with non-sparse matrix not yet implemented..')
            topWordInfo = self.getTopSimilarWords(doCosSim)
            print('The top ' + str(doCosSim) + ' globally most similar words are: ', end='')
            for n in topWordInfo:
                print(n.word, end=' ')
            print('')

    def doSVD(self, k): #TODO add an 'n' component for non-sparse
        """
        Performs singular value decomp on the current matrix
        :param k: The number of top features to extract
        #U now contains the eigenvectors of the correlation between the terms over the documents
            #Element (i,p) (which is equal to element (p,i)) contains the dot product \textbf{t}_i^T \textbf{t}_p ( = \textbf{t}_p^T \textbf{t}_i).
            #U is columns
        #V^t is the eigenvectors of the document products, giving doc-doc similarity
            #Vt is in rows. V (not Vt) consists of rows Dj
        #U = Term matrix (M,k)
        #S = Singular Values
        #Vt = SVD document matrix (k,N)
        """
        print('SVD into ' + str(k) + ' top features')
        if(self.isSparse):
            (self.U, self.S, self.Vt) = spsi.svds(self.dok_matrix, k=k)
        else:
            (self.U, self.S, self.Vt) = svd(self.matrix)
        self.V = self.Vt.transpose()

    def generateMatrix(self):
        """
        Generates this dataset's sparse or normal TF-IDF matrix depending on the value of self.isSparse
        :return:
        """
        self.matrixFill = 0
        if(self.isSparse):
            self.dok_matrix = dok_matrix((len(self.wordDict), self.articleCount), dtype=float)
            (self.matrixR ,self.matrixC) = self.dok_matrix.shape
            for (wNum, word) in enumerate(self.wordDict.keys()):
                buf = self.wordDict[word]
                for (articleNum, articleF) in buf.tfMap.items():
                    #NOTE doing the TFIDF transform as adding into the matrix
                    newEntry = (articleF / self.articleWC[articleNum]) * log(self.articleCount / self.wordDict[word].articleCount)
                    #self.matrix[wNum, articleNum] = newEntry
                    self.dok_matrix[wNum, articleNum] = newEntry
                    self.matrixFill += 1
        else:
            self.matrix = zeros([len(self.wordDict), self.articleCount]) # (r,c)
            (self.matrixR ,self.matrixC) = self.matrix.shape
            for (wNum, word) in enumerate(self.wordDict.keys()):
                buf = self.wordDict[word]
                for (articleNum, articleF) in buf.tfMap.items():
                    #NOTE doing the TFIDF transform as adding into the matrix
                    newEntry = (articleF / self.articleWC[articleNum]) * log(self.articleCount / self.wordDict[word].articleCount)
                    #self.matrix[wNum, articleNum] = newEntry
                    self.matrixFill += 1
        print('The density of the generated matrix is: ' + str(self.matrixFill) + ' / ' + str(len(self.wordDict) *  self.articleCount) + ' = %.5f' % (100 * self.matrixFill / (len(self.wordDict) * self.articleCount)) + ' percent')

    def getTopSimilarWords(self, n):
        """
        Returns a list of the top n most similar words based on cosineSimilarity
        :param n: The number of most similar words to return
        :return: A list of n similar words
        """
        related_word_indices = self.wordCosineSimilarity.argsort()[:-n:-1]
        print('Most Similar Word Indeces: ' + str(related_word_indices))
        topWordInfo = [] #Will consist of wordinfo
        valBuf = list(self.wordDict.values())
        for n in related_word_indices:
            topWordInfo.append(valBuf[n])
        return topWordInfo

    def getTopSimilarArticles(self, n):
        """
        Returns a list of the top n most similar articles based on cosineSimilarity
        :param n: The number of most similar words to return
        :return: A list of n similar articles
        """
        related_docs_indices = self.docCosineSimilarity.argsort()[:-n:-1]
        print('Doc Indeces: ' + str(related_docs_indices))
        topArticleInfo = []
        for n in related_docs_indices:
            topArticleInfo.append(self.getReutersArticle(n))
        return topArticleInfo

    def printTime(self):
        """
        Prints the time elapsed since this dataset's start_time
        :return:
        """
        s = time.time() - self.start_time
        if s > 60:
            m = math.floor(s / 60)
            print(' -T- It has been: %.0f' % m + ' minutes and %.3f seconds.' % (s % 60))
        else:
            print(' -T- It has been: %.3f seconds.' % (s % 60))

    def printWordDict(self):
        """
        Prints all words in the current wordDict(ionary)
        :return:
        """
        for w in range(len(self.wordDict)):
            print(self.matrix[w, :])    #TODO

    def printDictLong(self):
        """
        Prints all words in the current wordDict(ionary), as well as their total frequency, article counts, and frequencies per article
        :return:
        """
        for x in self.wordDict:
            buf = self.wordDict.get(x)
            print(x + ' occurs a total of ' + str(buf.totalFrequency) + ' times in ' +  str(buf.articleCount) + ' articles')
            for y in buf.tfMap:
                print(' ' + str(y) + ':' + str(buf.tfMap.get(y)), end = '')
            print('')
        print('The assembled word dictionary consists of ' + str(len(self.wordDict)) + ' words')

    def printDictMedium(self):
        """
        Prints all words in the current wordDict(ionary), as well as their article counts, and total frequency
        :return:
        """
        for x in self.wordDict:
            print('{' + x + ':' + str(self.wordDict.get(x).articleCount) + ':' + str(self.wordDict.get(x).totalFrequency) + '} ', end = '')
        print('')

    def printDictShort(self):
        """
        Prints the total number of words in the current wordDict
        :return:
        """
        print('Dictionary consists of ' + str(len(self.wordDict)) + ' words')

    def printWordCluster(self, c):
        """
        Prints the words of a word cluster
        :param c: The id of the word cluster to print
        :return:
        """
        print('\nWord Cluster #' + str(c))
        printWordList(self.clustersW(c))

    def printArticleCluster(self, c):
        """
        Prints (small) the articles of an article cluster
        :param c: The id of the article cluster to print
        :return:
        """
        print('\nArticle Cluster #' + str(c))
        printArticleListSmall(self.getArticleClusterInfo(c))

###########################################################################################

def cosine_similarity(v1, v2): # Copied from http://stackoverflow.com/questions/18424228/cosine-similarity-between-2-number-lists
    "compute cosine similarity of v1 to v2: (v1 dot v1)/{||v1||*||v2||)"
    sumxx, sumxy, sumyy = 0, 0, 0
    for i in range(len(v1)):
        x = v1[i]; y = v2[i]
        sumxx = sumxx + x*x
        sumyy = sumyy + y*y
        sumxy = sumxy + x*y
    return sumxy/math.sqrt(sumxx*sumyy) # 3x faster!


def plotMatrixPair(m1, m2):
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True, sharex=True)
    #cmap2 = cm.BrBG
    matplotlib.style.use('ggplot')
    #plt.set_cmap(cmap2)
    ax1.spy(m1, markersize=1, aspect='auto', origin='lower')
    ax2.spy(m2, markersize=1, aspect='auto', origin='lower')
    plt.xlabel('Articles')
    plt.ylabel('Words')
    plt.title('Sparse and Non-Sparse TF-IDF Matrices')
    plt.tight_layout()
    plt.show()

def plotMatrix(mat):
    plt.spy(mat, markersize=1, aspect='auto', origin='lower', marker='x')
    cmap2 = cm.BrBG
    plt.set_cmap(cmap2)
    plt.tight_layout()
    plt.show()

def plotHist(self, list, numBins):
    'Take a 1dimensional matrix or a list'
    plt.hist(list, bins=numBins)
    plt.show()


def printWordList(wordList):
    for word in wordList:
        word.printInfo()

def printArticleList(artList):
    for article in artList:
        article.printInfo()
        print('')

def printArticleListSmall(artList):
    for article in artList:
        article.printInfoSmall()
